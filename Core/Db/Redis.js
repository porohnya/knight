var redis = require('redis');

/**
 * @name Core_Db_Redis
 * @namespace
 * @constructor
 */
var Core_Db_Redis = function (host, port) {
    this._client = redis.createClient(port, host, {no_ready_check: true});
    this._client.on('error', this.onError.bind(this));
};


/**
 * @lends Core_Db_Redis
 */
Core_Db_Redis.prototype.extend({

    /**
     * Возвращает клиент
     * @returns {RedisClient}
     */
    client: function () {
        return this._client;
    },

    onError: function (error) {
        application.log.error('Redis error: ', error);
    }
});

module.exports = Core_Db_Redis;