'use strict';

/**
 * @name Core_Error_Codes
 * @namespace
 */
var Core_Error_Codes = {

    INVALID_EMAIL:              1,
    INVALID_PASSWORD:           2,
    LOGIN_ALREADY_EXISTS:       3,
    USER_NOT_FOUND:             4,
    WRONG_PASSWORD:             5
}

module.exports = Core_Error_Codes;