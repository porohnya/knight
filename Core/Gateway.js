'use strict';
var net = require('net'),
    http = require('http'),
    Core_Master_Client = require('Core/Master/Client'),
    Core_Log = require('Core/Log'),
    JsonSocket = require('json-socket');

var Core_Gateway = function (config, name) {
    this.config = config;
    /**
     * TCP server
     * @type {net.Server}
     */
    this.tcpServer = null;
    this.name = name;
    this.status = Core_Gateway.STATUS_NOT_CONNECTED;
    this.log = new Core_Log('Gateway <' + this.name + '>');
    this.log.trace('Starting gateway');
    this.masterClient = new Core_Master_Client(this.name, this.config.cluster.master.host, this.config.cluster.master.port);
}

Core_Gateway.prototype.extend({

    /**
     * Инициализация слейва
     */
    run: function () {
        if (!this.config.cluster.gateways[this.name]) {
            console.log('Wrong gateway configuration');
            process.exit(1);
        }

        this.masterClient.connectToMaster();
        this.masterClient.on('connect', this.onMasterConnect.bind(this));
        this.masterClient.on('close', this.onMasterClose.bind(this));
        this.masterClient.on('message', this.onMasterMessage.bind(this));
        this.listen();
    },

    /**
     * Запуск сервера
     */
    listen: function () {
        var self = this;
        /**
        this.server = http.createServer(function (req, res) {

            res.end('Gateway ' + application.name);
        }).listen(this.config.cluster.gateways[this.name].port, this.config.cluster.gateways[this.name].host);
        this.log.trace('listening on ' +  this.config.cluster.gateways[this.name].host + ':' + this.config.cluster.gateways[this.name].port);
        */
        var conf = this.config.cluster.gateways[this.name];
        this.tcpServer = net.createServer(function (connection) {
            self.log.trace('new incoming connection');

            connection.on('end', function () {
                self.log.trace('connection disconnected');
            });

            connection.write('Gateway ' + application.name);
            connection.pipe(connection);
        });
        this.tcpServer.listen(conf.port, function () {
            self.log.trace('TCP server listening on port ' + conf.port);
        })


    },

    /**
     * Отправка авторизации мастеру
     */
    authorization: function () {
        this.masterClient.send('authorization', {name: this.name, type: 2});
        setInterval(this.heartBeat.bind(this), 1000);
    },


    start: function () {
        this.status = Core_Gateway.STATUS_STARTED;
        this.log.trace('Slave started');
    },

    /**
     * Серцебиение
     */
    heartBeat: function () {
        var info = {
            uptime: Math.round(process.uptime()),
            memory: process.memoryUsage(),
            platform: process.platform,
            arch: process.arch,
            pid: process.pid,
            status: this.status
        };

        this.masterClient.send('heartbeat', {info: info});
    },

    /**
     * Подключение к мастеру осуществлено
     */
    onMasterConnect: function () {
        this.status = Core_Gateway.STATUS_CONNECTED;
    },

    /**
     * Ошибка в соединении с мастером
     * @param {Error} error
     */
    onMasterError: function (error) {
        process.exit(1);
    },

    /**
     * Закрылось соединение с мастером
     */
    onMasterClose: function () {
        process.exit(1);
    },

    /**
     * Ошибка сервера
     * @param {Error} error
     */
    onServerError: function (error) {
        this.log.error('server error:' + error.code || error);
        process.exit(1);
    },

    onServerMessage: function (message) {
        this.log.trace('message', message);
    },


    onServerConnect: function () {
        this.log.trace('new incoming connection');
    },

    /**
     * Сообщение от мастера
     * @param {Object}message
     */
    onMasterMessage: function (message) {
        switch (message.command) {
            case "authorization":
                this.authorization();
                break;
            case "authorized":
                this.status = Core_Gateway.STATUS_AUTHORIZED;
                break;
            case "start":
                this.start();
                break;
        }
    }
});

Core_Gateway.STATUS_NOT_CONNECTED = 1;
Core_Gateway.STATUS_CONNECTED = 2;
Core_Gateway.STATUS_AUTHORIZED = 3;
Core_Gateway.STATUS_STARTING = 4;
Core_Gateway.STATUS_STARTED = 5;
Core_Gateway.STATUS_STOPPING = 6;

module.exports = Core_Gateway;

