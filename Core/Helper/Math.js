'use strict';
var crypto = require('crypto');
/**
 * @name Core_Helper_Math
 * @namespace
 */
var Core_Helper_Math = {

    /**
     * Возвращает md5 от строки
     * @param {String} string
     * @returns {*}
     */
    md5: function (string) {
        return crypto.createHash('md5').update(string || '').digest("hex");
    }
}

module.exports = Core_Helper_Math;