'use strict';

var User = require('models/User'),
    Core_Helper_Math = require('Core/Helper/Math');
/*global application */

/**
 * @name Core_Helper_User
 * @namespace
 */
var Core_Helper_User = {

    /**
     * Регистрация
     * @param {String} login
     * @param {String} password
     * @param {Function} callback
     */
    register: function (login, password, callback) {
        var user = new User({
            login: login.toLowerCase(),
            password: this.generatePasswordHash(password),
            dateRegister: Date.nowTS()
        });

        user.authKey = this.generateKey(user);
        user.save(function (error) {
            if (error) {
                return callback(error);
            }
            callback();
        });
    },

    /**
    * Генерация ключа
    *
    * @param {User} user
    * @return String
        */
    generateKey: function (user) {
        var len = 16;
        var key = "";
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var i;
        for (i = 0; i < len; i += 1) {
            key += chars.substr(Math.floor(Math.random() * chars.length), 1);
        }
        return Core_Helper_Math.md5(key + (user.login || (user.socialId + user.networkId)));
    },

    /**
     * Генерация хеша для пароля
     *
     * @param {String} password
     * @returns {String}
     */
    generatePasswordHash: function (password) {
        return Core_Helper_Math.md5(Core_Helper_Math.md5(application.config.salt) + Core_Helper_Math.md5(password));
    }

};


module.exports = Core_Helper_User;