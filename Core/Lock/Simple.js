'use strict';

var async = require('async');

/*global application*/

/**
 * @name Core_Lock_Simple
 * @namespace
 */
var Core_Lock_Simple = {

    /**
     * Лочит
     * @param {String} name
     * @param {Function} callback
     */
    lock: function (name, callback) {
        async.waterfall([
            function (next) {
                application.redis.setnx(name, Date.nowTS(), next);
            },
            function (result, next) {
                if (!result) {
                    next('lock #' + name + ' has already exists');
                }
                application.redis.expire(name, 5, next);
            },
            function (result, next) {
                next();
            }
        ], callback)
    },

    /**
     * Разблокирует
     * @param {String} name
     * @param {Function} callback
     */
    unlock: function (name, callback) {
        async.waterfall([
            function (next) {
                application.redis.del(name, next);
            },
            function (num, next, a) {
                next();
            }
        ], callback);
    }
}

module.exports = Core_Lock_Simple;