'use strict';
var Syslog = require('node-syslog');


var Core_Log = function (logName) {
    this.level = global.config.log.level;
    this.name = logName;
    Syslog.init(global.config.log.name, Syslog.LOG_PID | Syslog.LOG_ODELAY, Syslog.LOG_LOCAL0);
}


Core_Log.prototype.extend({

    trace: function (msg) {
        if (this.level < 5) {
            return;
        }

        if (typeof msg === 'object'){
            msg = JSON.stringify(msg);
        }

        Syslog.log(Syslog.LOG_INFO, '['+ this.name +'] ' + msg);
    },


    error: function (msg) {
        if (this.level < 2) {
            return;
        }

        if (typeof msg === 'object'){
            msg = JSON.stringify(msg);
        }

        Syslog.log(Syslog.LOG_INFO, '['+ this.name +'] ' + msg);
    }
});



Core_Log.LEVEL_TRACE = 5;
Core_Log.LEVEL_INFO  = 4;
Core_Log.LEVEL_DEBUG = 3;
Core_Log.LEVEL_ERROR = 2;
Core_Log.LEVEL_NONE  = 1;

module.exports = Core_Log;