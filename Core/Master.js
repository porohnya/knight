'use strict';
var net = require('net'),
    JsonSocket = require('json-socket'),
    Core_Master_Connection = require('Core/Master/Connection'),
    Core_Log = require('Core/Log'),
    util = require('util'),
    Core_Master_Monitor = require('Core/Master/Monitor');

var Core_Master = function (config) {
    /**
     * Конфигурация
     * @type {Object}
     */
    this.config = config;

    /**
     * Подключенные соединения
     * @type {Object}
     */
    this.nodes = {};

    /**
     * Мастер сервер
     * @type {net.Server}
     */
    this.server = null;

    /**
     * Лог
     * @type {Core_Log}
     */
    this.log = new Core_Log('Master');


    /**
     * Запущен ли кластер
     * @type {boolean}
     */
    this.isStarted = false;
}

Core_Master.prototype.extend({

    /**
     * Запуск мастера
     */
    run: function () {
        Core_Master_Monitor.start(this.config);
        this.listen();

    },

    /**
     * Lister server
     */
    listen: function () {
        this.server = net.createServer();
        this.server.listen(this.config.cluster.master.port, this.config.cluster.master.host);
        this.log.trace('Server running ' + this.config.cluster.master.host + ':' + this.config.cluster.master.port);
        this.server.on('connection', this.onConnection.bind(this));
        this.server.on('error', this.onError.bind(this));
    },

    /**
     * Connection handler
     * @param {net.Socket} socket
     */
    onConnection: function (socket) {
        this.log.trace('New incoming connection');
        socket = new JsonSocket(socket);
        var connection = new Core_Master_Connection(socket);
        connection.send('authorization', {});
    },

    /**
     * Server Error handler
     * @param error
     */
    onError: function (error) {
        this.log.error('Master server error: ' + error.code || error);
        process.exit(1);
    },

    /**
     * Авторизирует коннекшн
     * @param {Core_Master_Connection} connection
     */
    authorizeConnection: function (connection) {
        if (connection.isAuthorized) {
            return;
        }

        this.log.trace('authorization new connection "' + connection.name + '"');

        if (this.nodes[connection.name]) {
            this.log.error('authorization failed');
            return;
        }

        this.log.trace('authorization success');
        connection.isAuthorized = true;
        connection.send('authorized', {});

        this.nodes[connection.name] = connection;
        if (this.isAllConnected()) {
            this.broadcast('start', {}, null);
            this.isStarted = true;
        }
    },


    /**
     * Броадкаст сообщения по слоям
     * @param {String} commandName
     * @param {Object} params
     * @param {Integer} layerType
     */
    broadcast: function (commandName, params, layerType) {
        Object.keys(this.nodes).forEach(function (nodeName) {
            if (this.nodes[nodeName].type === layerType || !layerType) {
                this.nodes[nodeName].send(commandName, params);
            }
        }, this);
    },


    /**
     * Проверяет все ли ноды подключились
     * @returns Boolean
     */
    isAllConnected: function () {
        var countSlaves = 0;
        var countGateways = 0;

        Object.keys(this.nodes).forEach(function (nodeName) {
            switch (this.nodes[nodeName].type) {
                case Core_Master_Connection.TYPE_SLAVE:
                    countSlaves += 1;
                    break;
                case Core_Master_Connection.TYPE_GATEWAY:
                    countGateways += 1;
                    break;
            }
        }, this);

        if (countSlaves === Object.keys(this.config.cluster.slaves).length) {

            return true;
        }

        return false;
    },

    /**
     * Запуск слейва
     * @param {String} slaveName
     */
    startSlave: function (slaveName) {
        //this.slaves[slaveName].send('start', {});
    }

});

module.exports = Core_Master;