'use strict';
var net = require('net'),
    JsonSocket = require('json-socket'),
    Core_Log = require('Core/Log'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter;


var Core_Master_Client = function (slaveName, masterHost, masterPort) {
    this.masterHost = masterHost;
    this.masterPort = masterPort;
    this.slaveName = slaveName;
    this.log = new Core_Log('MasterClient <' + slaveName + '>');
};

util.inherits(Core_Master_Client, EventEmitter);

Core_Master_Client.prototype.extend({

    /**
     * Подключается к мастеру
     */
    connectToMaster: function () {
        this.log.trace('Connecting to master ' + this.masterHost + ':' + this.masterPort);
        this.socket = new JsonSocket(new net.Socket());
        this.socket.connect(this.masterPort, this.masterHost);
        this.socket.on('connect', this.events.connect.bind(this));
        this.socket.on('error', this.events.error.bind(this));
        this.socket.on('message', this.events.message.bind(this));
        this.socket.on('close', this.events.close.bind(this));
    },


    /**
     * Отправка сообщения мастеру
     * @param {String} command
     * @param {Object} params
     */
    send: function (command, params) {
        //this.log.trace('sending to master command "' + command + '" with params ' + JSON.stringify(params));
        this.socket.sendMessage({command: command, params: params});
    },

    /**
     * События
     */
    events: {

        /**
         * Подключение установлено
         */
        connect: function () {
            this.log.trace('Connected to master');
            this.emit('connect');
        },


        /**
         * Ошибка
         */
        error: function (error) {
            this.log.error('Error connecting master: ' + error);
            this.emit('error', error);
        },

        /**
         * Подключение закрыто
         */
        close: function () {
            this.log.trace('Connection to master closed');
            this.emit('close');
        },

        /**
         * Сообщение получено
         */
        message: function (message) {
            this.log.trace('New message from master received: ' + JSON.stringify(message));
            this.emit('message', message);
        }
    }
});



module.exports = Core_Master_Client;