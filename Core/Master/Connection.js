'use strict';

var Core_Master_Connection = function (socket) {
    /**
     * Имя
     * @type {String}
     */
    this.name = null;

    /**
     * Тип
     * @type {null}
     */
    this.type = null;

    /**
     * Socket
     * @type {JsonSocket}
     */
    this.socket = socket;

    /**
     * Серцебиение
     * @type {number}
     */
    this.heartBeat = 0;

    /**
     * Авторизирован ли slave
     * @type {boolean}
     */
    this.isAuthorized = false;

    /**
     * Информация
     * @type {Object}
     */
    this.info = {};

    this.socket.on('message', this.onMessage.bind(this));
}

Core_Master_Connection.prototype.extend({

    send: function (commandName, params) {
        this.socket.sendMessage({command: commandName, params: params});
    },


    onMessage: function (message) {
        switch (message.command) {
            case 'authorization':
                this.name = message.params.name;
                this.type = message.params.type;
                master.authorizeConnection(this);
                break;
            case 'heartbeat':
                this.heartBeat = 0;
                this.info = message.params.info;
                break;
        }
    }

});

Core_Master_Connection.TYPE_SLAVE = 1;
Core_Master_Connection.TYPE_GATEWAY = 2;

module.exports = Core_Master_Connection;