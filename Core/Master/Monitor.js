'use strict';
var restify = require('restify'),
    Core_Log = require('Core/Log');

/**
 * @name Core_Master_Monitor
 * @namespace
 */
var Core_Master_Monitor = {

    _server: null,


    start: function (config) {
        this._config = config;
        this.log = new Core_Log('MasterMonitor');
        this._server = restify.createServer({version: '0.0.1', name: 'monitor'});
        this._server.use(restify.bodyParser({ mapParams: true }));
        this._server.use(restify.queryParser());
        this._server.use(
            function crossOrigin(req,res,next){
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Headers", "X-Requested-With");
                return next();
            }
        );
        this.initRouting();
        this._server.listen(config.cluster.master.monitor.port);
        this.log.trace('Started');
    },


    initRouting: function () {
        this._server.get({  path: '/'}, this.status);
    },

    /**
     * Статус сервера
     * @param req
     * @param res
     * @param callback
     */
    status: function (req, res) {
        var connectionsData = Object.keys(master.nodes).map(function (name) {
            return {
                name: name,
                isAuthorized: master.nodes[name].isAuthorized,
                heartbeat: master.nodes[name].heartBeat,
                type: master.nodes[name].type,
                info: master.nodes[name].info
            }
        })

        res.send({
            uptime: Math.round(process.uptime()),
            memory: process.memoryUsage(),
            slaves: connectionsData
        }, {'content-type': 'application/json; charset=utf-8'});


    }
}

module.exports = Core_Master_Monitor;