'use strict';
var net = require('net'),
    Core_Master_Client = require('Core/Master/Client'),
    Core_Log = require('Core/Log'),
    JsonSocket = require('json-socket'),
    util = require('util'),
    mongoose = require('mongoose'),
    pureautoinc  = require('mongoose-pureautoinc'),
    Core_Slave_HttpServer = require('Core/Slave/HttpServer'),
    Core_Slave_TcpServer = require('Core/Slave/TcpServer'),
    Core_Db_Redis = require('Core/Db/Redis');

var Core_Slave = function (config, name) {
    this.config = config;
    this.server = null;
    this.name = name;
    this.status = Core_Slave.STARTUS_NOT_CONNECTED;
    this.log = new Core_Log('Slave <' + this.name + '>');
    this.log.trace('Starting slave');
    this.masterClient = new Core_Master_Client(this.name, this.config.cluster.master.host, this.config.cluster.master.port);
    this.httpServer = new Core_Slave_HttpServer();
    this.tcpServer = new Core_Slave_TcpServer();
    this.controllers = {};
    this.redis = null;
}

Core_Slave.prototype.extend({

    /**
     * Инициализация слейва
     */
    run: function () {
        if (!this.config.cluster.slaves[this.name]) {
            console.log('Wrong slave configuration');
            process.exit(1);
        }

        this.masterClient.connectToMaster();
        this.masterClient.on('connect', this.onMasterConnect.bind(this));
        this.masterClient.on('close', this.onMasterClose.bind(this));
        this.masterClient.on('message', this.onMasterMessage.bind(this));

        var cfg = this.config.cluster.slaves[this.name];
        if (cfg.http) {
            this.httpServer.start(cfg.http);
        }

        if (cfg.tcp) {
            this.tcpServer.start(cfg.tcp);
        }
    },

    /**
     * Отправка авторизации мастеру
     */
    authorization: function () {
        this.masterClient.send('authorization', {name: this.name, type: 1});
        setInterval(this.heartBeat.bind(this), 1000);
    },

    /**
     * Обработка запуска слейва
     */
    start: function () {
        this.connectDb();
        this.connectRedis();
        this.status = Core_Slave.STATUS_STARTED;
        this.log.trace('Slave started');
    },

    /**
     * Устанавливает подключение с бд
     */
    connectDb: function () {
        var opts = {
            server: { auto_reconnect: true, primary:null },
            user: this.config.mongodb.user,
            pass: this.config.mongodb.password,
            primary: null
        };
        mongoose.connect('mongodb://'  + this.config.mongodb.user + ':' + this.config.mongodb.password + '@' + this.config.mongodb.host + ':' + this.config.mongodb.port +  '/' + this.config.mongodb.database);
        pureautoinc.init(mongoose.connection);
    },


    /**
     * Подключается к редису
     */
    connectRedis: function () {
        var redis = new Core_Db_Redis(this.config.redis.host, this.config.redis.port);
        this.redis = redis.client();
    },

    /**
     * Серцебиение
     */
    heartBeat: function () {
        var info = {
            uptime: Math.round(process.uptime()),
            memory: process.memoryUsage(),
            platform: process.platform,
            arch: process.arch,
            pid: process.pid,
            status: this.status
        };

        this.masterClient.send('heartbeat', {info: info});
    },

    /**
     * Подключение к мастеру осуществлено
     */
    onMasterConnect: function () {
        this.status = Core_Slave.STATUS_CONNECTED;
    },

    /**
     * Ошибка в соединении с мастером
     * @param {Error} error
     */
    onMasterError: function (error) {
        process.exit(1);
    },

    /**
     * Закрылось соединение с мастером
     */
    onMasterClose: function () {
        process.exit(1);
    },

    /**
     * Ошибка сервера
     * @param {Error} error
     */
    onServerError: function (error) {
        this.log.error('server error:' + error.code || error);
        process.exit(1);
    },

    /**
     * Сообщение от мастера
     * @param {Object}message
     */
    onMasterMessage: function (message) {
        switch (message.command) {
            case "authorization":
                this.authorization();
                break;
            case "authorized":
                this.status = Core_Slave.STATUS_AUTHORIZED;
                break;
            case "start":
                this.start();
                break;
        }
    },

    /**
     * Возвращает контроллер
     * @param {String} namespace
     * @param {String} controller
     */
    getController: function (namespace, controller) {
        if (!this.controllers[namespace] || !this.controllers[namespace][controller]) {
            if (this.registerController(namespace, controller)) {
                return this.getController(namespace, controller);
            } else {
                return false;
            }
        } else {
            return this.controllers[namespace][controller];
        }
    },


    /**
     * Регистрирует контроллер
     *
     * @param {String} namespace
     * @param {String} controller
     */
    registerController: function (namespace, controller) {
        this.log.trace('Registering controller ' + namespace + '/' + controller);
        if (!this.controllers[namespace]) {
            this.controllers[namespace] = {};
        }
        if (!this.controllers[namespace][controller]) {
            var filePath = 'layers/slave/controllers/' + namespace + '/' + controller;
            try {
                this.controllers[namespace][controller] = require(filePath);
            } catch (e) {
                this.log.error('Cannot register controller: ' + e + ' stackTrace: ' + e.stack);
                return false;
            }
        }
        return true;
    }
});

Core_Slave.STATUS_NOT_CONNECTED = 1;
Core_Slave.STATUS_CONNECTED = 2;
Core_Slave.STATUS_AUTHORIZED = 3;
Core_Slave.STATUS_STARTING = 4;
Core_Slave.STATUS_STARTED = 5;
Core_Slave.STATUS_STOPPING = 6;

module.exports = Core_Slave;

