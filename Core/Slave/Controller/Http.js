'use strict';

/**
 * @name Core_Slave_Controller_Http
 * @namespace
 * @param req
 * @param res
 * @constructor
 */
var Core_Slave_Controller_Http = function (req, res) {
    this.request = req;
    this.response = res;
    this.callback = null;
}

/**
 * @lends Core_Slave_Controller_Http.prototype
 */
Core_Slave_Controller_Http.prototype.extend({

    /**
     * Decay
     */
    decay: function () {
        this.request = null;
        this.response = null;
        this.callback = null;
    }
})

module.exports = Core_Slave_Controller_Http;