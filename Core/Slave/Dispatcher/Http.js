'use strict';

var routing = require('layers/slave/routing').http,
    Core_Slave_Controller_Http = require('Core/Slave/Controller/Http');
/**
 * @name Core_Slave_Dispatcher_Http
 * @namespace
 * @constructor
 */
var Core_Slave_Dispatcher_Http = function () {

    /**
     * Запрос
     */
    this.request = null;
    /**
     * Ответ
     */
    this.response = null;
    /**
     * Контроллер
     */
    this.controller = null;
    /**
     * Имя контроллера
     */
    this.controllerName = null;
    /**
     * Имя действия
     */
    this.actionName = null;
    /**
     * Полученные данные
     */
    this.receivedData = null;
    /**
     * Максимальное время выполнения
     * @type {number}
     */
    this.executionTime = 5;
}


Core_Slave_Dispatcher_Http.prototype.extend({

    /**
     * Обрабатывает запрос
     * @param req
     * @param res
     */
    process: function (req, res) {
        this.response = res;
        this.request = req;
        this.commandName = req.params.commandName || 'unknownCommand';
        this.response.dispatcher = this;

        var route = routing[this.commandName];
        if (!route) {
            this.handleError('Route not found');
            application.log.error('Route "' + this.commandName + '" not found');
        }

        this.controllerName = route.controller;
        this.actionName = route.action;

        application.log.trace('HTTP >>> "' + this.commandName + '": ' + JSON.stringify(req.params));

        if (route.executionTime) {
            this.executionTime = route.executionTime;
        }

        var controllerClass = application.getController('http', this.controllerName);
        if (!controllerClass) {
            this.handleError('Controller not found');
            application.log.error('Controller "' + this.controllerName + '" not found');
        }
        this.controller = new controllerClass(this.request, this.response);

        if (typeof this.controller[this.actionName + 'Action'] !== 'function') {
            this.handleError('Action not found');
            application.log.error('Action "' + this.actionName + '" not found in controller ' + this.controllerName);
        }

        this.controller.callback = this.handleAction.bind(this);
        this.controller[this.actionName + 'Action']();
        this.execTimer = setTimeout(this.limitExecutionTime.bind(this), this.executionTime * 1000);
    },


    /**
     * Лимит на выполнение запроса
     */
    limitExecutionTime: function () {
        application.log.error('Execution time limit: controller "' + this.controllerName + '" action "' + this.actionName+ '"');
        if (this.response) {
            this.handleError('Maximum execution time (' + this.executionTime + ' sec) has elapsed');
        }
    },


    /**
     * Обработка ошибки
     * @param error
     */
    handleError: function (error) {
        this.sendResponse({success: false, error: error});
    },

    /**
     * Обработка действия
     * @param {String} error
     * @param {Object} responseData
     */
    handleAction: function (error, responseData) {
        if (error) {
            return this.sendResponse({success: false, error: error});
        }
        this.sendResponse({success: true, data: responseData});

    },


    /**
     * Отправляет ответ на клиент
     * @param {Object} output
     */
    sendResponse: function (output) {
        output.server = application.name;
        this.response.send(output);
        clearTimeout(this.execTimer);

        if (this.controller) {
            this.controller.decay();
        }

        delete this.response.dispatcher;
        delete this.request;
        delete this.response;
        delete this.controller;
    }
});

module.exports = Core_Slave_Dispatcher_Http;