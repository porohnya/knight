'use strict';

var restify = require('restify'),
    Core_Slave_Dispatcher_Http = require('Core/Slave/Dispatcher/Http');

/**
 * @name Core_Slave_HttpServer
 * @namespace
 * @constructor
 */
var Core_Slave_HttpServer = function () {

    this._server = null;
}


Core_Slave_HttpServer.prototype.extend({

    /**
     * Запуск http-сервера
     */
    start: function (port) {
        this._server = restify.createServer({version: '0.0.1', name: 'monitor'});
        this._server.use(restify.bodyParser({ mapParams: true }));
        this._server.use(restify.queryParser());
        this._server.use(
            function crossOrigin(req, res, next){
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Headers", "X-Requested-With");
                return next();
            }
        );
        this._server.get({  path: '/http'}, this.dispatch);
        this._server.post({  path: '/http'}, this.dispatch);
        this._server.listen(port);
        application.log.trace('Http server started on port ' + port);
    },

    /**
     * Диспатчер
     * @param req
     * @param res
     */
    dispatch: function (req, res) {
        var dispatcher = new Core_Slave_Dispatcher_Http();
        dispatcher.process(req, res);
    }
});

module.exports = Core_Slave_HttpServer;