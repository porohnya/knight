'use strict';
var io = require('socket.io');
/*global application */
/**
 * @name Core_Slave_TcpServer
 * @namespace
 * @constructor
 */
var Core_Slave_TcpServer = function () {

    this._server = null;
}

/**
 * @lends Core_Slave_TcpServer.prototype
 */
Core_Slave_TcpServer.prototype.extend({

    /**
     * Запуск tcp-сервера
     */
    start: function (port) {
        this._server = io.listen(port);
        this._server.set('browser client', false);
        this._server.set('log level', 3);
        this._server.set('transports', ['websocket']);
        this._server.set('authorization', this._onAuthorization);
        this._server.sockets.on('connection', this._onConnection.bind(this));
        this._server.sockets.on('error', function (error) {
            console.log('error', error);
        })


        application.log.trace('Tcp Server started on port ' + port);
    },


    _onAuthorization: function (handshake, callback) {
        console.log('Authorization', application.name);
        callback(null, true);
    },

    _onConnection: function (socket) {
        console.log('_onConnection');
    },

    /**
     * Диспатчер
     * @param req
     * @param res
     */
    dispatch: function (req, res) {
       console.log(req);
        //res.end('OOK!');
    }

});


module.exports = Core_Slave_TcpServer