'use strict';

/**
 * @name Core_Validator_Abstract
 * @namespace
 * @constructor
 */
var Core_Validator_Abstract = function () {

    /**
     * Результат валидации
     * @type {{success: boolean, code: null}}
     */
    this.result = {
        success: false,
        code: null
    }
}


module.exports = Core_Validator_Abstract;