'use strict';

var validator = require('validator'),
    util = require('util'),
    async = require('async'),
    Core_Validator_Abstract = require('Core/Validator/Abstract'),
    Core_Error_Codes = require('Core/Error/Codes'),
    User = require('models/User'),
    Core_Helper_User = require('Core/Helper/User');

/**
 * @name Core_Validator_Abstract
 * @namespace
 * @constructor
 */
var Core_Validator_AuthUser = function () {
    Core_Validator_Abstract.call(this);
}

util.inherits(Core_Validator_AuthUser, Core_Validator_Abstract);

/**
 * @lends Core_Validator_AuthUser.prototype
 */
Core_Validator_AuthUser.prototype.extend({

    /**
     * Валидация
     * @param {String} login
     * @param {String} password
     * @param {Function} callback
     */
    validate: function (login, password, callback) {
        try {
            validator.check(login, Core_Error_Codes.INVALID_EMAIL).notNull().notEmpty().isEmail();
            validator.check(password, Core_Error_Codes.INVALID_PASSWORD).notNull().notEmpty().len(5);
        } catch (e) {
            this.result.success = false;
            this.result.code = e.message;
            return callback(null, this.result, null);
        }

        var self = this;
        async.waterfall([
            function (next) {
                User.findOne({login: login.toLowerCase()}, next);
            },
            function (result, next) {
                if (!result) {
                    self.result.success = false;
                    self.result.code = Core_Error_Codes.USER_NOT_FOUND;

                } else if (result.password !== Core_Helper_User.generatePasswordHash(password)) {
                    self.result.success = false;
                    self.result.code = Core_Error_Codes.WRONG_PASSWORD;
                } else {
                    self.result.success = true;
                }
                next(null, self.result, result);
            }
        ], callback);
    }
});


module.exports = Core_Validator_AuthUser;