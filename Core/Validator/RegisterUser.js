'use strict';

var validator = require('validator'),
    util = require('util'),
    async = require('async'),
    Core_Validator_Abstract = require('Core/Validator/Abstract'),
    Core_Error_Codes = require('Core/Error/Codes'),
    User = require('models/User');

/**
 * @name Core_Validator_Abstract
 * @namespace
 * @constructor
 */
var Core_Validator_RegisterUser = function () {
    Core_Validator_Abstract.call(this);
}

util.inherits(Core_Validator_RegisterUser, Core_Validator_Abstract);

/**
 * @lends Core_Validator_RegisterUser.prototype
 */
Core_Validator_RegisterUser.prototype.extend({

    /**
     * Валидация
     * @param {String} login
     * @param {String} password
     * @param {Function} callback
     */
    validate: function (login, password, callback) {
        try {
            validator.check(login, Core_Error_Codes.INVALID_EMAIL).notNull().notEmpty().isEmail();
            validator.check(password, Core_Error_Codes.INVALID_PASSWORD).notNull().notEmpty().len(5);
        } catch (e) {
            this.result.success = false;
            this.result.code = e.message;
            return callback(null, this.result);
        }

        var self = this;
        async.waterfall([
            function (next) {
                User.findOne({login: login.toLowerCase()}, next);
            },
            function (result, next) {
                if (result) {
                    self.result.success = false;
                    self.result.code = Core_Error_Codes.LOGIN_ALREADY_EXISTS;
                } else {
                    self.result.success = true;
                }
                next(null, self.result);
            }
        ], callback);
    }
});


module.exports = Core_Validator_RegisterUser;