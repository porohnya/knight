'use strict';

/*global application: true*/

var util = require('util');
var socketIO = require('socket.io');
var EventEmitter = require('events').EventEmitter;

/**
 * @name Core_Websocket_Server
 * @namespace
 * @constructor
 */
var Core_Websocket_Server = function () {

    /**
     * Websocket server
     *
     * @name Core_Websocket_Server._server
     * @type Object
     * @private
     */
    this._server = null;

};

util.inherits(Core_Websocket_Server, EventEmitter);

/**
 * @lends Core_Websocket_Server.prototype
 */
Core_Websocket_Server.prototype.extend({

    /**
     * Start a HTTP server listening
     *
     * @name Core_Websocket_Server.listen
     * @param {http.Server} httpServer
     */
    listen: function (httpServer) {
        this._server = socketIO.listen(httpServer);
        this._server.set('browser client', false);
        this._server.set('log level', 2);
        this._server.set('transports', ['websocket']);
        this._server.set('authorization', this._onAuthorization);
        this._server.sockets.on('connection', this._onConnection.bind(this));
    },

    /**
     * Authorization handler
     *
     * @method _onAuthorization
     * @param {Object} handshake
     * @param {Function} callback
     * @protected
     */
    _onAuthorization: function (handshake, callback) {
        console.log('_onAuthorization');
        /**
        var auth = new Game_Auth();
        auth.store = new Project_Auth_Store(handshake.query.sid);

        auth.getIdentity(function (err, identity) {
            if (!err) {
                if (identity) {
                    var user = application.storage.users.get(identity);
                    if (user) {
                        handshake.auth = auth;
                        handshake.user = user;
                        if (application.storage.users.updateTime) {
                            application.storage.users.updateTime(user._id);
                        }
                        callback(null, true);
                        return;
                    }
                    err = "User (" +  identity + ") not found in storage";
                } else {
                    err = "Session not found";
                }
            }

            console.log('Auth failed [' + handshake.query.sid + ']: ', err);
            callback(null, false);
        });
        */
    },

    /**
     * Connection handler
     *
     * @method _onConnection
     * @param {Object} socket
     * @protected
     */
    _onConnection: function (socket) {
        application.storage.wsConnections.add(socket.handshake.user, socket);
        delete socket.handshake.user;

        this.emit('connection', socket);
    }

});

module.exports = Core_Websocket_Server;