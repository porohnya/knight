module.exports = {
    development: {
        mongodb: {
            host: 'ds063287.mongolab.com',
            port: 63287,
            user: 'knight',
            password: 'knight',
            database: 'knight'
        },
        redis: {
            host: 'localhost',
            port: 6379
        },
        cluster: {
            master: {
                host: '127.0.0.1',
                port: '8001',
                monitor: {
                    port: 6001
                }
            },
            slaves: {
                sun:     {host: '127.0.0.1', tcp: 8002, http: 8022},
                venus:   {host: '127.0.0.1', tcp: 8003, http: 8023},
                earth:   {host: '127.0.0.1', tcp: 8004, http: 8024},
                mars:    {host: '127.0.0.1', tcp: 8005, http: 8025},
                uranium: {host: '127.0.0.1', tcp: 8006, http: 8026}

            }

        },
        log: {
            level: 5,
            name: 'cluster'
        },
        password: {
            salt: 'WTbXX9'
        }
    }
}