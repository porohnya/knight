require('overrides');

var Core_Gateway = require('Core/Gateway'),
    env = require('env'),
    config = require('config')[env];

global.config = config;
global.application = new Core_Gateway(config, process.argv[2]);
global.application.run();