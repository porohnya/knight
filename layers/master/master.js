require('overrides');

var Core_Master = require('Core/Master'),
    env = require('env'),
    config = require('config')[env];

global.config = config;
global.master = new Core_Master(config);
global.master.run();