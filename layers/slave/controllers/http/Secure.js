'use strict';

var Core_Slave_Controller_Http = require('Core/Slave/Controller/Http'),
    util = require('util'),
    async = require('async'),
    Core_Validator_RegisterUser = require('Core/Validator/RegisterUser'),
    Core_Validator_AuthUser = require('Core/Validator/AuthUser'),
    User = require('models/User'),
    Core_Helper_User = require('Core/Helper/User'),
    Core_Lock_Simple = require('Core/Lock/Simple'),
    Core_Helper_Math = require('Core/Helper/Math');

/**
 * @name SecureController
 * @namespace
 * @param req
 * @param res
 * @constructor
 */
var SecureController = function (req, res) {
    Core_Slave_Controller_Http.call(this, req, res);
}

util.inherits(SecureController, Core_Slave_Controller_Http);

/**
 * @lends SecureController.prototype
 */
SecureController.prototype.extend({

    /**
     * Авторизация
     */
    authAction: function () {
        var email = this.request.params.email,
            password = this.request.params.password,
            validator = new Core_Validator_AuthUser();
        async.waterfall([
            function (next) {
                validator.validate(email, password, next);
            },
            function (result, user, next) {
                if (!result.success) {
                    return next(null, result);
                }

                next(null, {
                    success: true,
                    akey: user.authKey,
                    uid: user._id,
                    login: user.login
                });
            }
        ], this.callback);

    },

    /**
     * Регистрация
     */
    registerAction: function () {
        var email = this.request.params.email,
            password = this.request.params.password,
            validator = new Core_Validator_RegisterUser(),
            lockName = 'LOCK_REGISTRATION' + Core_Helper_Math.md5(email);

        async.waterfall([
            function (next) {
                Core_Lock_Simple.lock(lockName, next);
            },
            function (next) {
                validator.validate(email, password, next);
            },
            function (result, next) {
                if (!result.success) {
                    return next(null, result);
                }

                Core_Helper_User.register(email, password, function (error) {
                    if (error) {
                        return next(error);
                    }

                    next(null, result);
                });
            },
            function (result, next) {
                Core_Lock_Simple.unlock(lockName, function () {
                    next(null, result);
                });
            }
        ], this.callback);
    }
});

module.exports = SecureController;