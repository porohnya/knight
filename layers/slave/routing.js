module.exports = {
    http: {
        registration:       {controller: 'Secure',      action: 'register'},
        auth:               {controller: 'Secure',      action: 'auth'}
    },
    tcp: {
        enterGame:          {controller: 'User',        action: 'enterGame'}
    }
}