require('overrides');

var Core_Slave = require('Core/Slave'),
    env = require('env'),
    config = require('config')[env];

global.config = config;
global.db = null;
global.application = new Core_Slave(config, process.argv[2]);
global.application.run();