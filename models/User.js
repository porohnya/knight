'use strict';

/*global application*/
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    pureautoinc = require('mongoose-pureautoinc');

/**
 * @name User
 * @namespace
 */
var UserSchema = new Schema({
    _id:                    {type: Number},
    name:                   {type: String,      default: null},
    login:                  {type: String,      default: null},
    password:               {type: String,      default: null},
    dateRegister:           {type: Number,      default: null},
    authKey:                {type: String,      default: null},
    isCharacterCreated:     {type: Boolean,     default: false}
}, {collection: 'User'});


UserSchema.plugin(pureautoinc.plugin, {
    model: 'User',
    field: '_id'
});



var UserModel = mongoose.model('User', UserSchema);
module.exports = UserModel;