var Knight = {
    helpers: {}
};

define([
    'router',
    'helpers/connection',
    'helpers/error'
], function (Router) {

    var initialize = function () {
        Knight.helpers.connection = ConnectionHelper;
        Knight.helpers.error = ErrorHelper;
        Router.initialize();
    }

    return {
        initialize: initialize
    }
})