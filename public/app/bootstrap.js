require.config({
    paths: {
        backbone: '../libraries/backbone-min',
        underscore: '../libraries/underscore-min',
        jquery: '../libraries/jquery.min',
        async: '../libraries/async',
        text: '../libraries/text',
        pnotify: '../libraries/pnotify/jquery.pnotify.min',
        jquerycookie: '../libraries/jquery.cookie',
        socketio: '../libraries/socket.io.min'
    },
    shim: {
        backbone: {
            deps: ["underscore", "jquery", "pnotify", "jquerycookie", "socketio"],
            exports: "Backbone"
        },

        underscore: {
            exports: "_"
        },

        socketio: {
            exports: 'io'
        },

        pnotify: {
            deps: ["jquery"],
            exports: 'jQuery.fn.pnotify'
        },

        jquerycookie: {
            deps: ["jquery"],
            exports: 'jQuery.fn.cookie'
        }
    }

});

require(
    ['app', 'async'],
    function (App, Async) {
        window.async = Async;

        window.alert = function(title, message) {
            $.pnotify({
                title: title,
                text: message
            });
        };
        App.initialize();
    }
);