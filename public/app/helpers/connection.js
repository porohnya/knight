var ConnectionHelper = {
    _socket:  null,

    connectWebsocket: function () {
        this._socket = io.connect('http://knight.local', {
            reconnect: false
        });
        this._socket.on('message', function (message) {
            console.log('ws socket message: ', message);
        });
        this._socket.on('error', function (error) {
            console.log('ws socket error', error);
        });
        this._socket.on('connect', function () {
            console.log('ws socket connected');
        })
    },


    sendHttpCommand: function (commandName, params, callback) {

        console.log('%c >>> ' + commandName + ' ', 'background: blue; color: white; border-radius:5px',params);

        _.extend(params, {commandName: commandName});

        $.ajax({
            url: '/http',
            data: params,
            type: 'POST',
            dataType: 'json',
            success: function (res) {
                if (res.success) {
                    console.log('%c <<< ' + res.server + ':' + commandName + ' ', 'background: green; color: white; border-radius:5px', res.data);
                    callback(null, res.data);
                } else {
                    console.log('%c <<< ' + res.server + ':' + commandName + ' ', 'background: red; color: white; border-radius:5px', 'SERVER ERROR: ' + res.error);
                    callback(null, 'Unknown server error');
                }


            },
            error: function () {
                callback('Unknown server error');
            }
        })
    }
}