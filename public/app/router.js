define([
    'backbone',
    'views/registration',
    'views/authorization'
], function(Backbone, RegistrationView, AuthorizationView){
    var AppRouter = Backbone.Router.extend({
        routes: {
            'registration': 'registration',
            'authorization': 'authorization'
        }
    });

    var initialize = function(){
        var app_router = new AppRouter;

        app_router.on('route:registration', function (actions) {
            var view = new RegistrationView();
            view.render();
        });

        app_router.on('route:authorization', function (actions) {
            var view = new AuthorizationView();
            view.render();
        });

        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});
