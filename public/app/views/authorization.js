define([
    'backbone',
    'text!templates/authorization.html'
], function(Backbone, template){
    var view = Backbone.View.extend({
        el: $('#root'),
        render: function(){
            var data = {};
            var compiledTemplate = _.template(template, data);
            this.$el.html(compiledTemplate);
        },

        events: {
            'click #btn-auth': 'authHandler'
        },


        authHandler: function () {
            $('#btn-auth').attr("disabled", "disabled");
            async.waterfall([
                function (next) {
                    Knight.helpers.connection.sendHttpCommand('auth', {
                        email: $('#input-auth-email').val(),
                        password: $('#input-auth-password').val()
                    }, next);
                },
                function (result, next) {
                    if (!result.success) {
                        window.alert('Произошла ошибка', Knight.helpers.error.getErrorByCode(result.code));
                    } else {
                        $.cookie("uid", result.uid,     {expires: 100, path: '/'});
                        $.cookie("login", result.login, {expires: 100, path: '/'});
                        $.cookie("akey", result.akey,   {expires: 100, path: '/'});
                        window.alert('Успешная авторизация', '<a href="#game">Начать игру</a>');
                    }
                    next()
                }
            ], function (){
                $('#btn-auth').removeAttr("disabled");
            });
        }
    });

    return view;
});
