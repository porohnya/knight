define([
    'backbone',
    'text!templates/registration.html'
], function(Backbone, template){
    var view = Backbone.View.extend({
        el: $('#root'),
        render: function(){
            var data = {};
            var compiledTemplate = _.template(template, data);
            this.$el.html(compiledTemplate);
        },

        events: {
            'click #btn-registration': 'registrationHandler'
        },



        registrationHandler: function () {
            $('#btn-registration').attr("disabled", "disabled");
            async.waterfall([
                function (next) {
                    Knight.helpers.connection.sendHttpCommand('registration', {
                        email: $('#input-registration-email').val(),
                        password: $('#input-registration-password').val()
                    }, next);
                },
                function (result, next) {
                    if (!result.success) {
                        window.alert('Произошла ошибка', Knight.helpers.error.getErrorByCode(result.code));
                    } else {
                        window.alert('Успешная регистрация', '<a href="#authorization">Начать игру</a>');
                    }
                    next()
                }
            ], function (){
                $('#btn-registration').removeAttr("disabled");
            });
        }
    });

    return view;
});
