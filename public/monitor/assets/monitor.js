var Monitor = {

    status: {
        2: 'connected',
        3: 'authorized',
        4: 'starting',
        5: 'started'
    },

    start: function () {
        setInterval(this._loadData.bind(this), 1000);
    },

    _loadData: function () {
        $.ajax({
            url: 'http://' + document.location.host + ':6001',
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function (data) {
                Monitor.writeData(data);
            }
        });
    },


    toTimeString: function (sec_num) {
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = hours+':'+minutes+':'+seconds;
        return time;
    },

    writeData: function (data) {
        var html = '<thead>' +
            '<tr>' +
            '<th></th>' +
            '<th>status</th>' +
            '<th>platform</th>' +
            '<th>arch</th>' +
            '<th>heartbeat</th>' +
            '<th>uptime</th>' +
            '<th>rss</th>' +
            '<th>heap used</th>' +
            '<th>heap total</th>' +
            '</tr>' +
            '</thead>';
        html += '<tr>' +
            '<td><span class="label label-inverse">MASTER</span></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td>' + this.toTimeString(data.uptime) + '</td>' +
            '<td>' + Math.round(data.memory.rss / 1024 / 1024) + ' MB</td>' +
            '<td>' + Math.round(data.memory.heapUsed / 1024 / 1024) + ' MB</td>' +
            '<td>' + Math.round(data.memory.heapTotal / 1024 / 1024) + ' MB</td>' +
            '</tr>';

        data.slaves.forEach(function (slave) {
            var label = 'label-info';
            if (slave.type == 1) {
                label = 'label-warning';
            }

            html += '<tr>' +
                '<td><span class="label ' + label +'">' + slave.name + '</span></td>' +
                '<td>' + this.status[slave.info.status] + '</td>' +
                '<td>' + slave.info.platform + '</td>' +
                '<td>' + slave.info.arch + '</td>' +
                '<td>' + slave.heartbeat + '</td>' +
                '<td>' + this.toTimeString(slave.info.uptime) + '</td>' +
                '<td>' + Math.round(slave.info.memory.rss / 1024 / 1024) + ' MB</td>' +
                '<td>' + Math.round(slave.info.memory.heapUsed / 1024 / 1024) + ' MB</td>' +
                '<td>' + Math.round(slave.info.memory.heapTotal / 1024 / 1024) + ' MB</td>' +
                '</tr>';
        }, this);

        $('#info').html(html);
    }
}

$(document).ready(function () {
    Monitor.start();
})


